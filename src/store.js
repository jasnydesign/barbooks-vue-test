import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	applications: []
  },
  mutations: {
	  addApplication (state, payload) {
	    state.applications.push(payload)
	    console.log(state.applications)
	  }
  },
  actions: {
	  addApplication ({commit}, payload) {
	    commit('addApplication', payload)
	  },
  },
  getters: {
  	getApplications: state => state.applications
  }
})
