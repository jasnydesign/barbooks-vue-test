import { shallowMount } from '@vue/test-utils'
import Application from '@/views/Application.vue'

describe('Application.vue', () => {
  it('renders props.msg when passed', () => {
    const formName = 'Rafał Jaseniuk'
    const formEmail = 'jasnydesign@gmail.com'
    const formSelectedSkillSingle = 'Vuejs'
    const wrapper = shallowMount(Application, {
      data: {
        formName: formName,
        formEmail: formEmail,
        formSelectedSkillSingle: formSelectedSkillSingle
      }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
